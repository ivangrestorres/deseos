import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { List } from '../../models/list.model';
import { ListItem } from '../../models/list-item.model';
import { DeseosProvider } from '../../providers/deseos/deseos';

/**
 * Generated class for the AgregarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregar',
  templateUrl: 'agregar.html',
})
export class AgregarPage {

  list: List;
  itemName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public deseosService: DeseosProvider) {
    const title = navParams.get("title");
    if (title){
      this.list = new List(title);
      deseosService.addList(this.list);
    }
    else{
      this.list = navParams.get("list");
    }
  }

  addItem() {
    if (this.itemName.length === 0){
      return;
    }
    else{
      this.list.items.push(new ListItem(this.itemName));
      this.deseosService.guardarListas();
      this.itemName = "";
    }
  }

  toggle() {
    let pendientes = this.list.items.filter(item => {
      return !item.completed;
    }).length;

    if (pendientes == 0){
      this.list.end = true;
      this.list.endAt = new Date();
    } else{
      this.list.end = false;
      this.list.endAt = null;
    }

    this.deseosService.guardarListas();
  }

  remove(id){
    this.list.items.splice(id, 1);
    this.deseosService.guardarListas();
  }
}
