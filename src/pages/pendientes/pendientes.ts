import { Component } from "@angular/core";
import { NavController, AlertController } from "ionic-angular";
import { AgregarPage } from '../agregar/agregar';

@Component({
    selector: 'page-pendientes',
    templateUrl: 'pendientes.html'
})
export class PendientesPage {
    constructor(public navCtrl: NavController, private alertCtrl: AlertController) {

    }
    
    goToAdd(){
        const prompt = this.alertCtrl.create({
            title: 'Login',
            message: "Enter a name for this new album you're so keen on adding",
            inputs: [
              {
                name: 'title',
                placeholder: 'Titulo'
              },
            ],
            buttons: [
              {
                text: 'Cancelar',
                handler: data => { }
              },
              {
                text: 'Agregar',
                handler: data => {
                    this.navCtrl.push(AgregarPage, {title: data.title});
                }
              }
            ]
          });
          prompt.present();
    }
}