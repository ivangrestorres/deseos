import { Injectable } from "@angular/core";
import { List } from '../../models/list.model';

@Injectable()
export class DeseosProvider {
  lists: List[] = [];
  constructor() {
    this.cargarListas()
  }

  addList(list:List){
    this.lists.push(list);
    this.guardarListas();
  }

  guardarListas() {
    localStorage.setItem('data', JSON.stringify(this.lists))
  }

  cargarListas() {
    if (localStorage.getItem('data')){
      this.lists = JSON.parse(localStorage.getItem('data'));
    }
    else {
      this.lists = [];
    }
  }

  remove(list: List) {
    const l = this.lists.filter((item:List) => {
      return item.id !== list.id;
    });
    this.lists = l;
    this.guardarListas();
  }
}
