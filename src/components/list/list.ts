import { Component, Input } from "@angular/core";
import { NavController } from "ionic-angular";
import { List } from "../../models/list.model";
import { AgregarPage } from "../../pages/agregar/agregar";
import { DeseosProvider } from '../../providers/deseos/deseos';

@Component({
  selector: "my-list",
  templateUrl: "list.html"
})
export class ListComponent {

  @Input() terminada: boolean = false;

  constructor(private navCtrl: NavController, public deseosProvider: DeseosProvider) {
  }

  selectedList(list: List) {
    this.navCtrl.push(AgregarPage, { list: list });
  }
}
